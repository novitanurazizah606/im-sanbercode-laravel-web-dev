<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$object = new Animal("shaun");
echo "Name : $object->name <br>";
echo "legs : $object->legs <br>";
echo "cold blooded : $object->cold_blooded <br>";

$object2 = new Frog("buduk");
echo "<br> Name : $object2->name <br>";
echo "legs : $object2->legs <br>";
echo "cold blooded : $object2->cold_blooded <br>";
echo "Jump : $object2->Jump <br>";

$object3 = new Ape("kera sakti");
echo "<br> Name : $object3->name <br>";
echo "legs : $object3->legs <br>";
echo "cold blooded : $object3->cold_blooded <br>";
echo "Yell : $object3->Yell <br>";